---
title: Introducción al _Diario de un seductor_ de Kierkegaard
diario: _Diario de un seductor_
diario_corto: _Diario_
kierkegaard: Kierkegaard
kierkegaard_completo: Søren Aabye Kierkegaard
kierkegaard_incompleto: Søren Kierkegaard
juan: don Juan
copen: Copenhague
uno_u_otro: _O lo uno o lo otro_
ironia: _Sobre el concepto de ironía en constante referencia a Sócrates_
estadios_eroticos: _Los estadios eróticos o el erotismo musical_
estadios: _Los estadios_
tags: intro, kierkegaard, diario de un seductor
toc:
  sec1: La historia imaginada del _Diario de un seductor_
  sec2: Las irónicas historias del _Diario_
  sec3: Algunas remembranzas familiares en _O lo uno o lo otro_
  sec4: La recepción de la obra de Kierkegaard
  sec5: Las voces de Kierkegaard desde un enclave feminista
  sec6: El don Juan kierkegaardiano para masculinidades contemporáneas
---

# Introducción al _Diario de un seductor_ de Kierkegaard

## La historia imaginada del _Diario de un seductor_

El 2 de febrero de 1843 en Copenhague, Dinamarca, se publicaron de manera conjunta y en danés los dos volúmenes de _O lo uno o lo otro_ de Søren Aabye Kierkegaard, un danés de familia acaudalada que nació en Copenhague el 5 de mayo de 1813. Acorde al testimonio del mismo autor, esta obra marcó el inicio de su carrera como escritor a sus 29 años, la cual presenta por primera vez al _Diario de un seductor_ en la parte final del primer volumen. Según la investigación de Rafael Larrañeta,[^1] esta primera edición constó de 525 ejemplares impresos en formato octavo (13 x 21 cm). Años después, en 1849, se imprimió una reedición de la cual Kierkegaard puso dinero de su propio bolsillo. Estas fueron las únicas dos ediciones de _O lo uno o lo otro_ que se publicaron en vida del autor danés.

<figure>

![
  Figura 1. Una carta de Cordelia a don Juan del manuscrito original del _Diario de un seductor_, c. 1841.
  (Fuente: Archivo de la Biblioteca Real de Dinamarca)
](
  local:figura1.png
  remoto:http://www5.kb.dk/en/nb/tema/webudstillinger/sk-mss/mss1/7.html
)
<figcaption>

Figura 1. Una carta de Cordelia a don Juan del manuscrito original del _Diario de un seductor_, c. 1841.
(Fuente: Archivo de la Biblioteca Real de Dinamarca)

</figcaption>
</figure>

Este breve recorrido editorial da comienzo a otras historias interesantes sobre las ediciones de _O lo uno o lo otro_ y el _Diario de un seductor_. Por un lado se tiene la historia imaginada por Kierkegaard. Por el otro, la dispar traducción y publicación en español de la obra de este filósofo.

Una de las características que más sobresalen de Kierkegaard como autor fue el uso de diversos seudónimos para firmar sus obras. Para el caso de _O lo uno o lo otro_, públicamente Kierkegaard negó su autoría, mientras que en sus diarios íntimos confesó ser su autor. Esta obra fue publicada por Victor Eremita, un ficticio editor con nombre latino que significa «victorioso ermitaño» o «el que vence en la soledad», según la traducción de Larrañeta.

Acorde al testimonio ficticio de este editor presente en el prólogo de _O lo uno o lo otro_, en 1835 le compró un secreter a un anticuario. Este mueble, en apariencia vacío, fue utilizado por Victor para albergar sus pertenencias. Un día de 1836, antes de salir de viaje, intentó sacar el dinero de un cajón del secreter. Por desgracia, se encontraba atascado por lo que en un momento de desesperación se dispuso a darle hachazos. No consiguió abrir el cajón de esta manera; sin embargo, debido a los golpes, descubrió un compartimiento secreto en el que Victor encontró un «montón de papeles».

Este editor se llevó los papeles consigo para su escrutinio, los cuales resultaron ser los manuscritos de _O lo uno o lo otro_. Según Victor, de manera evidente se percató que estos documentos fueron escritos por dos personas distintas. El primer personaje escribió de manera anónima una serie de tratados estéticos, mientras que el segundo escritor fue un tal Wilhelm, un asesor que trabajaba en algún tribunal y cuyos textos son unas amplias disquisiciones de contenido ético en respuesta al primer autor.

Para evitar confusiones, Victor decidió llamar a ambos escritores como A y B, respectivamente. Sobre los papeles de B, este supuesto editor menciona que fue fácil ordenarlos, ya que consisten en tres largas epístolas. Sin embargo, para el caso de los escritos de A la tarea no fue sencilla. Debido a su estilo dispar, Victor decidió dejar los papeles de A en la disposición en como los encontró, en plena ignorancia de si «este orden tiene un valor cronológico o un significado ideal».

Sobre los textos de A, Victor hace notar que consisten en escritos que pueden agruparse de la siguiente manera. En primer lugar se cuentan con varios aforismos que comienzan con un epígrafe en latín, «_ad de ipsum_» (a sí mismo), que según Larrañeta hace referencia al ejemplar de las _Meditaciones_ de Marco Aurelio que tuvo Kierkegaard. El segundo grupo de escritos consisten en «ensayos estéticos», acorde a la apreciación de Victor. Entre ellos destaca el ensayo sobre la figura de don Juan en la obra de Mozart, compositor e intérprete que Kierkegaard apreció de modo especial. El último texto de A consiste en el _Diario de un seductor_, supuestamente escrito en 1834 y cuyo epígrafe inicial es un fragmento del _Don Juan_ de Mozart. Según la opinión de Victor, si bien el escritor A solo declara ser el editor del _Diario_, tiene sospechas de que se trate de una «vieja artimaña de narrador, contra la cual yo nada tendría que objetar».

Victor indicó que terminó de ordenar los papeles en 1837, aunque decidió guardarlos por cinco años hasta finalmente llevarlos a la imprenta a finales de 1842, para comenzar su distribución en febrero de 1843. Para su publicación Victor tuvo que escoger el título de la obra. Como se tratan de dos autores contrapuestos, el primero que habla de manera estética sobre la vida, mientras que el segundo le responde en una actitud ética, este ficticio editor decidió el título de _O lo uno o lo otro_ para hacer explícita ambas nociones contrapuestas sobre la existencia y de las cuales no hay manera de constatar que alguno de estos escritores hubiese convencido al otro.

## Las irónicas historias del _Diario_

La historia imaginada de Kierkegaard en el que implica a cuatro personajes ---a saber: Victor Eremita, Wilhelm (escritor B), el escritor A y el autor del _Diario_---, permiten evidenciar el uso de los nombres de autor que el mismo filósofo percibe como «un juego de cajas chinas», en el cual cada caja reside dentro de otra como si se tratasen de muñecas rusas. Entre sus diversos lectores no hay consenso sobre los objetivos precisos de semejante uso de nombres, aunque sí se percibe la _ironía_ en ello.

El término «ironía» tiene un significado particular en la obra de Kierkegaard, debido a que fue el tema de _Sobre el concepto de ironía en constante referencia a Sócrates_, su tesis de maestría que defendió con éxito en 1841. En esta tesis, Kierkegaard concibe la ironía como aquello que «oscila entre el Yo ideal y el Yo empírico», a través de la cual «el sujeto se libera de las ataduras». Por lo que «la ironía es la primera y más abstracta determinación de la subjetividad» ya que «a nada le da seriedad». Es decir, «hace y deshace cualquier cosa a voluntad», en donde el ejercicio de la libertad es proporcional al empleo de la ironía.

Un ejemplo de esta manera de entender a la ironía lo tenemos en los personajes que Kierkegaard utiliza para explicar la génesis de _O lo uno o lo otro_. Por un lado tenemos a un don Juan anónimo que con «artimañas» se deslinda de la paternidad del _Diario de un seductor_. Por el otro, Wilhelm toma con seriedad lo dicho por el escritor A, hasta el punto de afirmar que «Dios instituyó el matrimonio porque no era bueno que el hombre estuviese solo», por lo que le recomienda al don Juan a dejar de ser «una mueca de ser humano». Sin embargo, las voces de estos personajes no serían conocidas sin el trabajo de Victor Ermita. La ironía consiste en que es un victorioso ermitaño, una persona más allá de lo uno o lo otro, quien habilita los discursos de un esteta y un eticista.

Otro ejemplo de semejante noción kierkegaardiana de la ironía se puede observar en la manera en como los editores y traductores han tratado la obra de Kierkegaard. Según Jaime Franco Barrio,[^2] la obra de este filósofo fue conocida más allá de Dinamarca a partir de las traducciones que se llevaron a cabo a partir del siglo XX. Las primeras traducciones se hicieron en alemán, francés, inglés e italiano. Las primeras ediciones en español hicieron su aparición aproximadamente en 1918, aunque la proliferación de publicaciones tuvo su apogeo entre los cincuenta y sesenta, cien años después de sus ediciones originales.

El carácter irónico de las traducciones en español reside en un comentario patente en el trabajo de Larrañeta: el trato dado por los editores los cuales incurrieron en notables cambios, entre los que destacan la fragmentación de los textos, la mezcla de obras y la publicación selectiva de párrafos. La ironía consiste en que, a pesar de la exigencia erudita de tener en español la obra íntegra de Kierkegaard, el acceso a nuestra lengua de este filósofo ha sido gracias al libre ejercicio de los editores. Sin ese trabajo solo tendríamos noticias de Kierkegaard a través de referencias o por los comentarios de sus lectores en otras lenguas.

La historia imaginada de Kierkegaard sobre _O lo uno o lo otro_ dista mucho de las historias reales de sus ediciones en español. No obstante, estas comparten el pleno uso de la ironía por parte de sus editores. Mientras que el ficticio Victor Ermita dio «orden» y tituló un «montón de papeles» para su publicación, con esta misma ausencia de ataduras es como varios editores en español han publicado diversos apartados de _O lo uno o lo otro_ con distintos títulos o de manera fragmentaria.

Para el caso de las ediciones del _Diario de un seductor_ en español, Franco localiza su primera edición independiente en el trabajo elaborado por Santiago Rueda en Buenos Aires en 1951. Dos años después el _Diario_ apareció dentro de la primera parte de _La alternativa_ ---título para _O lo uno o lo otro_ seleccionado por el editor---, publicada en Madrid por Espasa-Calpe como parte de su Colección Austral. A partir de ahí se hicieron otras ediciones del _Diario_ entre las que destacan la publicada por Ediciones 29 en Barcelona en 1971, la de Guadarrama que dio a la luz en Madrid en 1976 ---la cual incluye a _Temor y temblor_, otra obra de Kierkegaard---, así como la edición de Fontamara publicada en Barcelona en 1980.

<figure>

![
  Figura 2. Portada de la primera edición al español del _Diario de un seductor_, 1951.
  (Fuente: todocolección)
](
  local:figura2.png
  remoto:https://www.todocoleccion.net/libros-segunda-mano-literatura/soren-kierkegaard-diario-un-seductor-santiago-rueda-editor~x199509618
)
<figcaption>

Figura 2. Portada de la primera edición al español del _Diario de un seductor_, 1951.
(Fuente: todocolección)

</figcaption>
</figure>

En la actualidad el _Diario de un seductor_ se continua publicando por diversas editoriales, en su mayoría consisten en un remix de ediciones anteriores. Para contribuir a esta diversidad bibliográfica, la obra que tienes entre tus manos consiste en una nueva traducción hecha por Eduardo Sánchez Gómez con el objetivo de conservar su integridad. Con ello se busca mantener viva la ironía kierkegaardiana sobre su propia obra: el libre criterio de los editores de disponer de sus textos con el fin de hacerlo más accesible a sus lectores.

## Algunas remembranzas familiares en _O lo uno o lo otro_

Además del uso irónico de varios seudónimos, otra singularidad adicional de la obra de Kierkegaard es su marcado acento autobiográfico. Acorde a Larrañeta, _O lo uno o lo otro_ fue escrito y corregido durante dieciséis meses después de haber defendido su tesis de maestría, entre octubre de 1841 y enero de 1843. Según Jorge del Palacio Martín,[^3] el _Diario de un seductor_ fue escrito entre el otoño de 1841 y el invierno de 1842. Esta obra la escribió durante su estancia en Berlín mientras asistía a las lecciones de Schelling,[^4] en las cuales tuvo por compañeros a von Humboldt,[^5] Engels[^6] y Bakunin,[^7] aunque no haya entablado amistad con ellos. El motivo por el que acudió a las clases del anciano Schelling fue para escuchar su crítica a Hegel;[^8] sin embargo, como atestigua en sus diarios, el curso le pareció decepcionante.

Meses antes de partir a Berlín, en agosto, Kierkegaard tomó una decisión que influyó en su trabajo, principalmente a _O lo uno o lo otro_. Después de casi un año de compromiso con Regine Olsen ---hija de un conocido burócrata danés que Kierkegaard conoció de manera accidental en una reunión de colegialas en 1837, cuando ella tenía 15 años y él, 24---, rompe su relación con el envío de una carta que contenía su anillo de compromiso. En esta epístola Kierkegaard se declara incompetente para hacerla feliz. Regine fue afectada por esta ruptura hasta el punto de que su padre le exigió a Kierkegaard que se presentase para atender la situación. Este encuentro tuvo lugar días antes de su partida a Berlín.[^9]

<figure>

![
  Figura 3. Retrato de Regine Olsen hecho por Emil Bærentzen, c. 1840.
  (Fuente: Wikimedia Commons)
](
  local:figura3.png
  remoto:http://alturl.com/qofb4
)
<figcaption>

Figura 3. Retrato de Regine Olsen hecho por Emil Bærentzen, c. 1840.
(Fuente: Wikimedia Commons)

</figcaption>
</figure>

Aunque años después tuvieron encuentros casuales, en 1847 Regine se casó con Frederik Schlegel, un reconocido abogado. A partir de entonces Schlegel le impidió a Kierkegaard ver a Regine y en 1855 esta pareja se marchó de Dinamarca debido a que Schlegel fue elegido gobernador de las Indias Occidentales Danesas, una colonia localizada en el Caribe. Ahí terminó la historia de amor para Kierkegaard ya que Regine regresó a Dinamarca hasta 1860, cinco años después de su muerte. Para su amada esta historia se prolongó después del fallecimiento de su marido, en 1896, a través de diversas entrevistas que dio a biógrafos y comentaristas de Kierkegaard.

Muchas cosas se han dicho sobre este episodio de la vida de Kierkegaard y Regine. Como este filósofo fue conocido entre sus connacionales como un severo crítico de la Iglesia de Dinamarca y un profundo creyente cristiano, críticos como Franco y Palacio rescatan una sugestiva interpretación bíblica al respecto. El 16 de octubre de 1843 Kierkegaard publicó _Temor y temblor_ bajo el seudónimo de Johannes de Silentio, nombre latino que se traduce por «Juan del Silencio». Ahí, el filósofo danés medita sobre la misión que Dios le encomendó a Abraham de sacrificar a su hijo Isaac. Este designio divino presenta un problema ético: si bien el asesinato es un acto inmoral, Abraham obvia esta cuestión debido a la fe que tiene en Dios. Por fortuna para Abraham, Dios interviene antes de consumar el acto. Para infortunio de Kierkegaard, este relato se interpreta como el sacrificio que hizo para demostrar su devoción por Dios sin que este lo detuviese.

Cordelia es el personaje por el cual gravita el _Diario de un seductor_, debido a que es el objeto de seducción del don Juan de esta obra. La manera en como ambos personajes se relacionan tiene un eco en el modo en como Kierkegaard cortejó y rompió con Regine. Por ejemplo, don Juan conoció a Cordelia en una fiesta de jovencitas de manera semejante a como Kierkegaard vio por primera vez a Regine. Además, el acecho a Cordelia por parte de don Juan resuena en el testimonio que hizo Kierkegaard en sus diarios al mencionar que desde una ventana veía pasar a Regine sin que ella lo supiese. Otro paralelismo se presenta cuando Kierkegaard le declaró su amor a Regine, la cual no dio respuesta inmediata sino días después y a través de su padre; para el caso de don Juan, Cordelia guardó silencio hasta que su tía emitió su consentimiento. El intercambio epistolar entre don Juan y Cordelia es un recordatorio sobre la larga correspondencia que Kierkegaard y Regine mantuvieron hasta el fin de su compromiso. También los personajes del _Diario_ comparten con los enamorados una ruptura abrupta y poco comprensible. Por último, las constantes menciones de los rizos de Cordelia hacen que ella tenga un parentesco a los retratos conocidos de Regine.

Con estas remembranzas familiares es como _O lo uno o lo otro_ se articula a partir de acontecimientos autobiográficos que detonaron las reflexiones estéticas y éticas de las cuales Kierkegaard obtendría un lugar en las historias de filosofía. A partir del dolor autoinfligido de Kierkegaard es como su pensamiento trascendió la singularidad de su vida.

## La recepción de la obra de Kierkegaard

El 2 octubre de 1855, Kierkegaard se desmayó mientras caminaba por la calle. De inmediato fue llevado al hospital. El 11 de noviembre, sin poderse recuperar, el filósofo danés falleció a los 42 años. Se desconoce la causa de su muerte, aunque las especulaciones indican estas posibilidades: complicaciones por una caída que sufrió en su juventud ---de la cual Kierkegaard señalaba como la causa de su joroba---, la enfermedad de Pott ---un tipo de tuberculosis que se caracteriza por el crecimiento de una joroba---, sífilis, meningitis, o epilepsia.

Kierkegaard fue conocido en Copenhague debido a su crítica a la Iglesia de Dinamarca, sus libros publicados y sus excentricidades, como su aspecto físico, su forma de vestir y su capacidad de conversar con desconocidos. Esta fama hizo que su muerte no pasara desapercibida. En su funeral asistió una multitud, en su mayoría personas que Kierkegaard entabló una amistad durante sus cuantiosas caminatas y charlas. Entre los asistentes fueron contadas las personas religiosas, universitarias o literatas.[^10]

<figure>

![
  Figura 4. Caricatura de Kierkegaard ilustrada por Wilhelm Marstrand, c. 1870.
  (Fuente: Archivo de la Biblioteca Real de Dinamarca)
](
  local:figura4.png
  remoto:http://www5.kb.dk/en/nb/tema/webudstillinger/sk-mss/sk-portraetter/marstrand.html
)
<figcaption>

Figura 4. Caricatura de Kierkegaard ilustrada por Wilhelm Marstrand, c. 1870.
(Fuente: Archivo de la Biblioteca Real de Dinamarca)

</figcaption>
</figure>

Aunque varios de sus contemporáneos reconocieron las capacidades intelectuales y literarias de Kierkegaard, la recepción de su obra fue póstuma. Esto pudo deberse a los siguientes factores. Primero, su desdén o incapacidad de relacionarse con sus críticos u otros pensadores, como fueron sus adversarios del clero o los célebres personajes con quienes compartió aula. Segundo, una barrera idiomática porque su obra estaba en danés, una lengua poco común para los cánones occidentales. Tercero, el provincialismo de su vida en el sentido de que no perfiló su carrera en los centros intelectuales de Europa, sino en su ciudad natal. Cuarto, el empleo de seudónimos dificultó la recopilación e intelección de su trabajo como obra de un único autor. Quinto, su estilo se consideró de difícil comprensión por su editor o sus lectores, como atestiguan Franco, Begonya Saez Tajafuerce[^11] y Darío González.[^12]

En 1901 empieza a publicarse su obra completa en danés. Esta primera edición se constituye de 14 volúmenes, posteriores ediciones la ampliaron a 20. Estos volúmenes se comenzaron a traducir primero al alemán, trabajo que concluyó en 1923. En el resto de los idiomas sus textos empezaron a ser traducidos de manera selectiva, como es caso de sus ediciones al español.

Miguel de Unamuno[^13] fue quien a principios del siglo XX introdujo a Kierkegaard en habla hispana. Unamuno leyó casi por completo su obra editada en danés y lo citó o fue centro de sus reflexiones en diversos escritos, aunque no tradujo ningún texto. Con ello se tiene evidencia de que la recepción de la obra de Kierkegaard en español fue relativamente temprana y fragmentaria a comparación de otros idiomas.

La inclusión de la obra de Kierkegaard en el canon filosófico occidental fue a partir de los comentarios, críticas y reflexiones de célebres filósofos existencialistas.[^14] Por este motivo es que de manera usual se le denomina como el «padre del existencialismo». Si bien es cierto que Kierkegaard se considera un pensador de la existencia, esta autopercepción no hace referencia a su póstuma inclusión dentro de la corriente filosófica del existencialismo. Por ende hay que tener cautela en comparar su entendimiento de la existencia con las reflexiones existencialistas del siglo XX.

La obra de Kierkegaard tiene como punto de partida su crítica al pensamiento hegeliano, paradigma filosófico imperante de su tiempo. Por esta oposición se le ha colocado al lado de Nietzsche.[^15] Sin embargo, vale de nuevo advertir que esta es una disposición posterior que da pie a equívocos. Aunque ambos filósofos comparten un ferviente teísmo juvenil y un distanciamiento de Hegel, los caminos a los que llegaron son inconmensurables. El alejamiento en Nietzche fue a través de la crítica a la moral cristiana cuya cumbre es la muerte de dios. Mientras que para Kierkegaard fue la defensa de la singularidad individual que culmina en un salto de fe al estadio religioso en pos de un compromiso con dios.

Entonces, un camino recomendado para abordar su obra parte de las siguientes diferencias conscientes que tiene respecto a Hegel. Por un lado, un antisistematismo que lo llevó a usar distintos seudónimos y por el cual, en la pretensión de encontrar una constante en su pensamiento, se obtienen argumentos dispares e incompatibles entre sí. Por otro, su hincapié en rehabilitar la singularidad de la vida individual, en lugar de subordinar la existencia a una fenomenología hegeliana del espíritu. Por último, su énfasis en que la discontinuidad de las decisiones personales hacen imposible reducir la vida a una intrincada lógica dialéctica. En este punto de partida, el _Diario de un seductor_ es la obra más accesible para comenzar a rastrear los intereses kierkegaardianos por la singularidad y ambivalencias humanas a partir de lo que denomina estadios estéticos y éticos.

## Las voces de Kierkegaard desde un enclave feminista

En nuestro tiempo la obra de Kierkegaard ha sido recibida críticamente por feministas y personas interesadas en los estudios de género o de masculinidades. En el _Diario de un seductor_ y otros textos las voces seudónimas de Kierkegaard emiten distintas opiniones sobre las mujeres o la feminidad. Debido a que una de las dificultades de la obra de Kierkegaard es poder encontrar una coherente y única voz de autor, no hay consenso al momento de valorar estas aseveraciones.

Por ejemplo, Amelia Valcárcel[^16] valora a Kierkegaard como «el mejor representante de la misoginia galante». Por «galantería» esta filósofa se refiere a «la concepción romántica del amor cortés». Mientras que por «misoginia» hace referencia a la descalificación del «colectivo de las mujeres a base de ideas o rasgos generales menospreciativos supuestamente compartidos por todas». El foco de la crítica de Valcárcel es en la manera en como Kierkegaard asocia a la mujer con el «sexo débil» o el «sexo bello» por el cual no puede ser para sí misma, sino solo para otros, donde su liberación depende de los varones o su fuerza recae en su masculinización.

Esta interpretación misógina sobre la mujer es naturalista y esencialista ya que el «ser mujer» se concibe como anclado de manera inherente a su sexo y feminidad, así como el «ser varón» es un modo de ser ontológicamente distinto. Esta manera de entender a las mujeres y los varones percibe su diferenciación sexual a partir de características exclusivas y excluyentes para cada uno. Por ejemplo, la singularidad kierkegaardiana de la cual se distancia del hegelianismo es una característica propia de los varones porque la mujer que aspira a esta existencia individual «se vuelve repugnante y digna de mofa», debido a que el supuesto fin de las mujeres es la otredad, cuya máximo designio es el de acompañar al varón.

<figure>

![
  Figura 5. Retrato inacabado del galante y singular Søren Aabye Kierkegaard hecho por su primo Niels Christian Kierkegaard, c. 1840.
  (Fuente: Wikimedia Commons)
](
  local:figura5.png
  remoto:http://alturl.com/c838k
)
<figcaption>

Figura 5. Retrato inacabado del galante y singular Søren Aabye Kierkegaard hecho por su primo Niels Christian Kierkegaard, c. 1840.
(Fuente: Wikimedia Commons)

</figcaption>
</figure>

En _O lo uno o lo otro_ y en el _Diario_ hay cuantiosas referencias que dan significado y sentido a la crítica de Valcárcel. Sin embargo, con la misma obra Vanessa Bowns Poulsen[^17] tiene una valoración menos severa sobre Kierkegaard. Para Poulsen las opiniones kierkegaardianas sobre la mujer han de entenderse como un punto de vista común en las sociedades del siglo XIX. Además, por la diversidad de seudónimos es imposible concluir que estas opiniones hayan sido las que Kierkegaard sostuvo sino, tal vez, una manera irónica de razonar. Para dar fundamento a sus argumentos, Poulsen destaca que en el _Diario de un seductor_ también es perceptible una visión igualitaria y emancipatoria sobre el género, como cuando don Juan reflexiona sobre la necesidad de modificar la currícula para darle más oportunidades a las mujeres.

No obstante, es necesario matizar el liberalismo presente en el _Diario_. Por un lado, cuando este don Juan sugiere una educación que tome en cuenta la «sagacidad de las jóvenes», lo hace para proponer una clase que les enseñe a entablar y terminar relaciones. Por otro lado, si bien es cierto que para «su tiempo» puede considerarse progresista, para «nuestro tiempo» estas iniciativas se considerarían limitadas y conservadoras, más cercanas a la visión decimonónica de «educación doméstica» que de una educación liberadora o igualitaria.

Esta falta de consenso sobre el valor de la obra de Kierkegaard entre feministas o personas estudiosas del género no implica que su criticidad adolezca de coherencia o relevancia. Al contrario, la disputa contemporánea sobre la misoginia en Kierkegaard evidencia uno de los grandes logros del movimiento feminista: la capacidad de releer textos antiguos desde nuevas perspectivas, en lugar de conformarnos con la repetición o el asentimiento de opiniones y valoraciones ya dadas.

Además, la pluralidad de juicios que se han dado en este enfoque sobre los textos kierkegaardianos expone una cuestión que la mayoría de los medios o personas obvian al hablar del feminismo: este movimiento aglutina una diversidad de posturas que en varias ocasiones no comparten objetivos, valores o ideas, por lo que su cohesión se ha articulado mediante la tolerancia, la autocrítica y el deseo de un mundo sin opresión basada en el sexo o género. En la actual masificación de este movimiento es importante tener en cuenta su disparidad de posiciones. De lo contrario, se corre el riesgo de mutar la tolerancia en discursos de odio o misandria ---término análogo a la misoginia pero para descalificar al colectivo de los varones; vale la pena advertir el doble sentido de este vocablo porque también se ha empleado con el propósito de descalificar las exigencias feministas---, la crítica en ideología y el deseo en nuevas formas de opresión, como ha pasado con otros movimientos políticos, sociales o filosóficos.

## El don Juan kierkegaardiano para masculinidades contemporáneas

El _Diario de un seductor_ permite una reflexión sobre las masculinidades a través de las andanzas de don Juan. Su interpretación de esta figura en el _Diario_ difiere de manera significativa del don Juan de la obra de Mozart presente en el epígrafe inicial de esta novela. La cantidad de mujeres seducidas por el don Juan de Mozart son 1,003, en donde jamás parece enamorarse o entablar una relación tendida con alguna de sus conquistas.[^18] Mientras tanto, al don Juan kierkegaardiano solo se le conocen dos mujeres seducidas, una de ellas es Cordelia, el personaje por el cual se justifica la existencia del _Diario_.

<figure>

![
  Figura 6. Diseño de vestuario para la obra _Don Juan_ de Mozart, s. f.
  (Fuente: Wikimedia Commons)
](
  local:figura6.png
  remoto:https://commons.wikimedia.org/wiki/File:Don_Juan_costume_designs.jpg
)
<figcaption>

Figura 6. Diseño de vestuario para la obra _Don Juan_ de Mozart, s. f.
(Fuente: Wikimedia Commons

</figcaption>
</figure>

En _Los estadios eróticos o el erotismo musical_, un texto del primer volumen de _O lo uno o lo otro_, Kierkegaard analiza la figura de don Juan. Ahí indica que este personaje tiene sus antecedentes en el cristianismo del medievo ---aunque al parecer ignoró que su primera incursión en la literatura se dio en España durante el siglo XVII---. Una de las características que llaman su atención es su carácter ridículo debido a la imposibilidad de que un individuo logre seducir a más de mil mujeres. Esta exageración la atribuye a que don Juan es más bien el símbolo de «la fuerza de la naturaleza y lo demoníaco» en donde el número 1,003 denota el vigor en plenitud de esta quimera. En este punto, Kierkegaard señala que hay que tomar con reservas la seducción de esta figura, ya que para él la seducción implica un acto reflexivo que no está presente en el don Juan de Mozart.

Por ello, a Kierkegaard le parece que esta figura es tanto cómica y poco bella como impostora, porque para seducir es necesario recurrir a la mentira, la cual concibe como el ejercicio consciente de la palabra. A partir de ahí es como el filósofo danés determina que el don Juan es una idea que en un sentido musical ha sido interpretada sin igual por Mozart, pero como literatura no ha encontrado una concepción que trate el carácter reflexivo y psicológico de la seducción. Después de _Los estadios eróticos o el erotismo musical_, en el _Diario de un seductor_ encontramos la propuesta kierkegaardiana de un don Juan menos absurdo: un personaje que durante toda la novela se dedica a seducir reflexivamente a Cordelia, mientras encuentra un goce instantáneo en María, una sirvienta que se encuentra en un parque.

En _Los estadios_, Kierkegaard observa que el fundamento de la «fuerza» del don Juan reside en el apetito sensual e idealizante donde «en cada mujer desea a toda la feminidad», por lo que «esta es la razón de que para él se desvanezcan todas las diferencias particulares ante el hecho principal: ser mujer». En una de las reflexiones finales y más extensas del _Diario_ es perceptible de nueva cuenta esta noción ontologizante de la mujer cuando Kierkegaard menciona que cada una es tan solo un «esbozo», una «pequeña parte», de la totalidad femenina. Aquí hay una cita velada al mundo de las ideas platónicas que Kierkegaard conoció a profundidad por sus estudios sobre Sócrates, ya que la obra de Platón es una de las principales fuentes sobre este personaje.

Una interpretación del mundo de las ideas explica que la relación entre las ideas y las cosas ---«hechos» para Kierkegaard--- es mediante la participación ---«emanaciones» para el danés--- por la cual cada cosa es la expresión de las ideas que encarna, aunque al mismo tiempo no son las ideas en sí. Estas permanecen en una dimensión ontológica distinta al mundo en el que vivimos, por lo que cada cosa cabe entenderse como un «esbozo», una «parte mínima», de las ideas. Una consecuencia de esta ontología kierkegaardiana sobre la mujer es su aplicabilidad a cualquier sexo o género. En un mundo platónico no hay cosa por sí misma sino que todas son por la participación de las ideas que encarnan. Es decir, desde esta perspectiva los varones también son un mero esbozo y un hecho incapaz de fundarse a sí mismo sino a través de otros, como el desgraciado Eduardo, el personaje que don Juan utiliza para llegar a Cordelia. La masculinidad y la feminidad así entendidas son ideas de las cuales únicamente son posibles abarcar a pedazos.

Con el don Juan de Kierkegaard se despoja el carácter ridículo de una virilidad que fantasea con seducir una gran cantidad de mujeres. Con su explicación de la participación de cada mujer con el ideal de la feminidad se obtiene una perspectiva sesgada sobre la incompletud del ser para sí y en participación con una totalidad masculina o femenina. El don Juan de Kierkegaard nos deja como tarea pendiente la reelaboración de esta figura mediante una trama que dedique copiosas páginas para ahora reflexionar sobre el carácter incompleto, fantasioso y estereotípico de la masculinidad. Quizá es en nuestro tiempo donde valga la pena la interpretación de un don Juan cuyas andanzas se articulen a partir de la otredad, en lugar de percibirlas como triunfos de su seducción.

[^1]: Rafael Larrañeta Olleta (Navarra, 1945 -- Madrid, 2002) fue doctor en Filosofía y Teología. Se desempeñó como profesor titular de Filosofía Política en la Facultad de Filosofía de la Universidad Complutense de Madrid. Su interés por Kierkegaard lo llevó a aprender danés para su lectura. Varios años de su vida los dedicó al estudio del filósofo danés y a traducirlo en español. Su investigación sobre Kierkegaard lo llevó a publicar varios libros como _Una moral de la felicidad_ (1979), _La preocupación ética_ (1986), _Lecciones para la clase de Utopía_ (2000) y _La lupa de Kierkegaard_ (2002). Sobre su trabajo junto con otros colaboradores, él mismo asegura en ser los primeros en editar de manera íntegra las obras de Kierkegaard. El resultado fue publicado por editorial Trotta en diversos volúmenes bajo el nombre de _Escritos_ de Kierkegaard. Después de algunos años de vivir con una grave enfermedad, muere en compañía de familiares y amigos en diciembre de 2002. Para el presente texto se utilizó su «Presentación» en _Escritos 2 de Kierkegaard. O lo uno o lo otro. Un fragmento de vida I_, Trotta, Madrid, 2006.

[^2]: Poco fue posible saber sobre Jaime Franco Barrio, excepto que es o fue doctor en Filosofía por la Universidad de Salamanca y que completó sus estudios en la Universidad de München. Sobre Kierkegaard publicó «Kierkegaard en español» en _Azafea: revista de filosofía_ (1989), «Kierkegaard en el “Brand” de Ibsen» en _Revista Estudio agustiniano_ (1990) y _Kierkegaard frente al hegelianismo_ (1996). Para este texto se utilizó la primera publicación mencionada.

[^3]: Jorge del Palacio Martín es licenciado en Filosofía por la Universidad de Deusto y doctor en Ciencia Política y de la Administración por la Universidad Autónoma de Madrid. Es especialista en ideologías políticas, teoría política e historia política contemporánea, en particular, del sistema político español e italiano. También es columnista en varios periódicos o revistas como _El País_, _El Mundo_ o _Letras Libres_. Para el presente texto se emplea su «Introducción» en _Diario de un seductor_, Alianza editorial, Madrid, 2008.

[^4]: Friedrich Schelling (1775--1854) fue un filósofo alemán representante del idealismo y romanticismo alemán. Por su precoz madurez intelectual, a sus dieciséis años pudo ingresar al seminario de Tubinga, una célebre residencia estudiantil e institución educativa de la Iglesia protestante. Ahí hizo amistad con Hegel, quien junto con Fichte influenció y dio soporte a su carrera intelectual. A partir de 1807 se distanció de Hegel después de que este le pidiese un prefacio para su _Fenomenología del espíritu_, debido a que Schelling consideró que hacia mofa de su trabajo. Después de varios años como profesor universitario, muere a los 79 años en Suiza. Sus obras más destacadas son _Del Yo como principio de la filosofía_ (1795), _Sistema del idealismo trascendental_ (1800), _Investigaciones filosóficas sobre la esencia de la libertad humana y los objetos con ella relacionados_ (1809) y _Las edades del mundo_ (1811--1815). En caso de mención explícita, para esta y las siguientes semblanzas cfr. sus fichas biográficas en Wikipedia.

[^5]: Alexander von Humboldt (1769--1859) fue un humanista, naturalista y explorador prusiano. Él se considera como uno de los fundadores de la geografía como ciencia empírica. Sus viajes lo llevaron a Europa, América y Asia Central, en donde realizó diversos experimentos y estudios de campo. Su trabajo lo hizo ser reconocido por varias instituciones como la Real Sociedad de Londres, la Sociedad Filosófica Estadunidense, la Academia de las Ciencias de Berlín y la Academia francesa. Respecto a su asistencia a las lecciones de Schelling no emitió un juicio tan áspero como el de Kierkegaard; sin embargo, tampoco aceptó su concepción naturalista. Muere a los 89 años de manera pacífica en su domicilio. Sus dos obras más célebres son _Viaje a las regiones equinocciales del Nuevo Continente_ (1799--1804) y _Cosmos_ (1845--1862, los últimos volúmenes fueron publicados póstumamente).

[^6]: Friedrich Engels (1820--1895) fue un filósofo y politólogo comunista alemán conocido por haber sido amigo y editor de Karl Marx. Hijo de prósperos industriales textiles, en 1841 se trasladó a Berlín para prestar su servicio militar. Fue así como asistió a las clases de Schelling, a quien criticó con severidad en un panfleto que publicó en 1842. De esta manera tuvo su primer encuentro poco amistoso con Marx. Pese a este desencuentro, ambos personajes limaron sus asperezas, lo que les permitió cuarenta años de intensa colaboración. Engels murió de cáncer en el esófago a los 74 años. Entre sus obras más conocidas están _La sagrada familia_ (1844, escrita en coautoría con Marx), _Del socialismo utópico al socialismo científico_ (1880), _El origen de la familia, la propiedad privada y el Estado_ (1884) y _Ludwig Feuerbach y el fin de la filosofía clásica alemana_ (1888).

[^7]: Mijaíl Bakunin (1814--1876) fue un filósofo y teórico ruso que se considera uno de los padres del anarquismo. Como desertor del ejército ruso debido a su indisciplina, en 1834 se trasladó a Moscú para estudiar filosofía en donde desarrolló una admiración por Fichte y Hegel. Con el objetivo de conseguir una cátedra en la Universidad de Moscú, en 1840 migró a Berlín para continuar con su preparación. De esta manera es como asistió a las lecciones de Schelling, a quien le hizo saber su desacuerdo sobre el carácter idealista de su pensamiento. Pese a ello, Schelling fue una base que le ayudó a distanciarse de una interpretación esencialista de la obra hegeliana. Por sus ideales políticos fue expulsado de varios países europeos, además de provocarle más de una fricción con Marx y de entablar una relación con Pierre-Joseph Proudhon, otro padre del anarquismo. Entre la pobreza y el retiro de sus actividades políticas, fallece en Suiza a los 62 años. Dos de sus obras más sobresalientes son _Estatismo y anarquía_ (1873) y _Dios y el Estado_ (1882).

[^8]: Georg Wilhelm Friedrich Hegel (1770--1831) fue un célebre filósofo alemán cuyo legado es considerado tan importante como el de René Descartes o el de Immanuel Kant. Por su pensamiento sistemático la obra hegeliana abarca tópicos de la filosofía, el derecho, la lógica, la teoría política o la epistemología. Su trabajó ha permeado a varios pensadores hasta nuestros días como Søren Aabye Kierkegaard, Karl Marx, Friedrich Nietzsche, Martin Heidegger, Jean-Paul Sartre, Georges Bataille, Theodor W. Adorno, Hans-Georg Gadamer, Jacques Lacan, Jacques Derrida, Jürgen Habermas, Judith Butler, Slavoj Žižek, entre otros. Célebre entre sus contemporáneos, impartió cátedras en diversas universidades. Al parecer muere a los 61 años por cólera. Entre sus obras seleccionadas destacan _Fenomenología del espíritu_ (1807), _Ciencia de la lógica_ (1812--1816), _Elementos de la filosofía del derecho_ (1821) y _Lecciones sobre la estética_ (1820--1829).

[^9]: Esta anécdota fue recuperada del prefacio escrito por John Updike (1932--2009), escritor estadunidense, en _The Seducer's Diary_, Editorial de la Universidad de Princeton, Nueva Jersey, 1997.

[^10]: Las posibles causas de su muerte y el relato de su funeral están documentados en la entrada sobre Kierkegaard de la Wikipedia en danés.

[^11]: Begonya Saez Tajafuerce es doctora en Filosofía por la Universidad Autónoma de Barcelona, así como profesora y coordinadora de estudios de máster y doctorado del Departamento de Filosofía en la misma universidad. Su investigación se centra en la definición y la representación de la identidad. Forma parte del Grupo de Investigación Cuerpo y Textualidad, que, desde una perspectiva interdisciplinaria, analiza el cuerpo como lugar para la identidad y la manera como se inscriben en él el género, la etnia, la clase y la identidad sexual desde los discursos culturales. Ha editado _Cuerpo, memoria y representación: Adriana Cavarero y Judith Butler en diálogo_ (2014). Esta semblanza fue extraída de su ficha en el sitio del Centro de Cultura Contemporánea de Barcelona. Para este texto se empleó su «Introducción» en _Escritos 1 de Kierkegaard. De los papeles de alguien que todavía vive. Sobre el concepto de la ironía_, Trotta, Madrid, 2000.

[^12]: Por desgracia no fue posible localizar alguna semblanza de Darío González. Se sabe de su trabajo sobre Kierkegaard debido a su «Estudio introductorio. Søren Kierkegaard, pensador de la subjetividad» en _Kierkegaard_, Gredos, Madrid, 2010. Este extenso estudio fue utilizado para el presente texto.

[^13]: Miguel de Unamuno (1864--1936) fue un escritor y filósofo español que cultivó una gran variedad de géneros literarios. Hijo de una familia de comerciantes de Bilbao, en 1880 se trasladó a Madrid para sus estudios universitarios en filosofía y letras. A sus veinte años empezó a ejercer como profesor en diversos institutos hasta llegar a ser rector de la Universidad de Salamanca desde 1901 hasta su muerte. Fue en ese cargo donde recibió con entusiasmo la obra de Kierkegaard. En 1935 fue uno de los finalistas para el Premio Nobel de Literatura, aunque se declaró desierto. Al parecer la Alemania nazi presionó a la Fundación Nobel para que Unamuno no recibiera el premio debido a su postura antifascista. Después del golpe de Estado hecho por Francisco Franco en 1936, Unamuno fue destituido como rector y puesto bajo arresto domiciliario. Ahí es donde a sus 72 años muere de manera súbita el 31 de diciembre de 1936. La obra de Unamuno es extensa, entre los títulos más destacados están _Vida de don Quijote y Sancho_ (1905), _Niebla_ (1907), _Abel Sánchez_ (1917), _La tía Tula_ (1921) y _San Manuel Bueno mártir_ (1930). Los textos que más dedicó a Kierkegaard son «Ibsen y Kierkegaard» en _Mi religión y otros ensayos breves_ (1910) y _Del sentimiento trágico de la vida_ (1913).

[^14]: Karl Jaspers, Martin Heidegger y Jean-Paul Sartre fueron los filósofos existencialistas que más entablaron un diálogo con la obra de Kierkegaard. Para conocer de manera general las ideas kierkegaardianas que tuvieron más adopción cfr. Michael Theunissen, «El perfil filosófico de Kierkegaard» en _Estudios de filosofía_, Universidad de Antioquia, Antioquia, 2005; Ángel Viñas Vera, «Notas sobre la antropología de Kierkegaard» en _Daimon. Revista Internacional de Filosofía_, Universidad de Murcia, Murcia, 2016; Cristian Eduardo Benavides, «La crítica kierkegaardiana a la concepción hegeliana de libertad según la interpretación de Cornelio Fabro» en _Areté. Revista de Filosofía_, Pontificia Universidad Católica de Perú, Lima, 2016.

[^15]: Friedrich Nietzsche (1844--1900) fue un filósofo y filólogo alemán considerado como uno de los pensadores occidentales más influyentes. Criado en una familia de un pastor luterano, en 1864 empezó sus estudios en teología y filología. Al poco tiempo se decantó de la teología y se acercó a la filosofía de Arthur Schopenhauer. Años después ejerció como profesor de filología clásica y comenzó a publicar los libros que poco a poco le dieron fama de filósofo. Sus relaciones personales fueron complejas, al parecer por su carácter. En 1889 tuvo un colapso mental que lo llevó a la locura. En ese estado muere a sus 55 años el 25 de agosto de 1900 debido a una neumonía. Sus libros más importantes son _El nacimiento de la tragedia_ (1872), _Humano, demasiado humano_ (1878), _La gaya ciencia_ (1882), _Así habló Zaratustra_ (1883), _Más allá del bien y del mal_ (1886), _La genealogía de la moral_ (1887), _El Anticristo_ (1888), _El ocaso de los ídolos_ (1889) y _Ecce homo_ (1889).

[^16]: Amelia Valcárcel (1950) es una doctora en Filosofía y teórica feminista española. Además de catedrática y escritora, ha desempeñado diversos cargos públicos para el gobierno español. Por ejemplo, fue Consejera de Educación, Cultura, Deportes y Juventud del Principado de Asturias (1993--1995) y desde 2006 es Consejera de Estado de España. Cuenta con diversos premios y distinciones como la medalla de Asturias (2006), un doctorado _honoris causa_ por parte de la Universidad Veracruzana (2015) por su «sobresaliente trayectoria profesional y académica en los campos de la filosofía y feminismo» o la Gran Cruz de la Orden Civil de Alfonso X el Sabio (2016), presea que se entrega a personas con méritos excepcionales. Entre sus obras más publicitadas están _Hegel y la ética_ (1989), _Sexo y filosofía, «mujer y poder»_ (1991), _La política de las mujeres_ (1997), _Ética contra estética_ (1998), _Rebeldes_ (2000), _Feminismo en un mundo global_ (2009), _La memoria y el perdón_ (2011) y _Ahora, feminismo. Cuestiones candentes y frentes abiertos_ (2019). Para este texto se usó «Misoginia romántica. Hegel, Schopenhauer, Kierkegaard, Nietzsche» en _La filosofía contemporánea desde una perspectiva no androcéntrica_, Ministerio de Educación, Cultura y Deporte, Madrid, 1993.

[^17]: Lo único que fue posible conocer sobre Vanessa Bowns Poulsen es su artículo que en este escrito se emplea como referencia: «Søren Kierkegaard's The Seducer's Diary: The Socratic Seduction of a Young Woman» en _Revista Filosofiska Notiser_, 2016.

[^18]: En youtu.be/nV1yNgiEvIQ hay una excelente grabación subtitulada al español de esta ópera de Mozart que se interpretó en el Teatro Comunale de Ferrara en 1997. La producción la hizo Lorenzo Mariani y la dirección musical fue de Claudio Abbado. Para una interpretación contemporánea de esta obra está _El ojo del diablo_ (1960), un filme de Ingmar Bergman que con un poco de empeño es posible obtener una copia digital subtitulada. Una interpretación más cercana es _Sólo con tu pareja_ (1991), la ópera prima de Alfonso Cuarón que también está disponible en YouTube.